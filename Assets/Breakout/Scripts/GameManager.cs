﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public static GameManager instance;

    PlayerPrefs highScoreKeeper;

    public BrickController brickPrefab;
    public BallController ballController;
    public int rows = 5;
    public int columns = 10;
    public float edgePadding = 0.1f;
    public float bottomPadding = 0.4f;

    public AudioSource soundLoop;


    List<BrickController> brickList = new List<BrickController>();
    public int brickCount
    {
        get { return brickList.Count; }
    }

    public Text livesUI;
    public Text scoreUI;
    public Text highscoreUI;
    public Text gameOverUI;
    public Text winUI;

    public int score = 0;
    public int lives = 3;
    public int highScore;

    private GameObject[] getCount;

    void Awake()
    {
        if (instance == null)
        {
            Debug.Log("Yeah!");
            instance = this;

        }
        else
        {
            Debug.Log("oh no");
        }

        Debug.Log("My Score: " + score);
        Debug.Log("Manager Score: " + instance.score);
    }

    private void Start()
    {
        livesUI.text = "Lives: " + lives;
        instance.scoreUI.text = "Score: " + instance.score;
        instance.highscoreUI.text = "High Score: " + PlayerPrefs.GetInt("highScore");
        instance.ballController.gameObject.SetActive(true);
        CreateBricks();
        Debug.Log("The Saved High Score is" + PlayerPrefs.GetInt("highScore"));
        Debug.Log("The current high score is" + highScore);

        soundLoop.enabled = true;
        Debug.Log(soundLoop.isActiveAndEnabled + " Audio Source Data");
        


    }

    private void Update()
    {
        HighScoreChanger();
        
        //drawing soundwave to debugger
        float[] spectrum = new float[256];

        soundLoop.GetSpectrumData(spectrum, 0, FFTWindow.Rectangular);

        for (int i = 1; i < spectrum.Length - 1; i++)
        {
            Debug.DrawLine(new Vector3(i - 1, spectrum[i] + 10, 0), new Vector3(i, spectrum[i + 1] + 10, 0), Color.red);
            Debug.DrawLine(new Vector3(i - 1, Mathf.Log(spectrum[i - 1]) + 10, 2), new Vector3(i, Mathf.Log(spectrum[i]) + 10, 2), Color.cyan);
            Debug.DrawLine(new Vector3(Mathf.Log(i - 1), spectrum[i - 1] - 10, 1), new Vector3(Mathf.Log(i), spectrum[i] - 10, 1), Color.green);
            Debug.DrawLine(new Vector3(Mathf.Log(i - 1), Mathf.Log(spectrum[i - 1]), 3), new Vector3(Mathf.Log(i), Mathf.Log(spectrum[i]), 3), Color.blue);
        }
        //end of soundwave drawing
    }

    void CreateBricks()
    {
        Vector3 bottomLeft = Camera.main.ViewportToWorldPoint(new Vector3(edgePadding, bottomPadding, 0));
        Vector3 topRight = Camera.main.ViewportToWorldPoint(new Vector3(1 - edgePadding, 1 - edgePadding, 0));

        bottomLeft.z = 0;

        float w = (topRight.x - bottomLeft.x) / (float)columns;
        float h = (topRight.y - bottomLeft.y) / (float)rows;

       
        for(int row = 0; row < rows; row++)
        {
            for(int col = 0; col < columns; col++)
            {
                BrickController brick = Instantiate(brickPrefab) as BrickController;
                brick.transform.position = bottomLeft + new Vector3((col + .5f) * w, (row + .5f) * h, 0);
                //do other stuff to brick
                brickList.Add(brick);
            }
        }
    }

    public static void LostBall()
    {
        instance.lives -= 1;
        instance.livesUI.text = "Lives: " + instance.lives;
        if (instance.lives < 0)
        {
            instance.gameOverUI.text = "You Lose";
            instance.gameOverUI.gameObject.SetActive(true);
            instance.livesUI.text = "Lives: 0";
            HighScoreSaver();
            
        }
    }

    public static void BrickBroken(int points)
    {
        instance.score += points;
        instance.scoreUI.text = "Score: " + instance.score;

        bool hasWon = true;
        for (int i = 0; i < instance.brickList.Count; i++)
        {
            BrickController brick = instance.brickList[i]; 
            if (brick.gameObject.activeSelf)
            {
                hasWon = false;
                break;
            }

        }

        if (hasWon)
        {
            instance.winUI.text = "You Win";
            instance.winUI.gameObject.SetActive(true);
            instance.ballController.gameObject.SetActive(false);
            HighScoreSaver();
            

        }

        
    }

    public static void HighScoreChanger()
    {
        if (instance.score > instance.highScore)
        {
            instance.highScore = instance.score;
            
        }

        if (instance.highScore > PlayerPrefs.GetInt("highScore"))
        {
            instance.highscoreUI.text = "High Score: " + instance.highScore;
        }
    }

    public static void HighScoreSaver()
    {
        Debug.Log("The HighScoreSaver() function was called");

        if (PlayerPrefs.HasKey("highScore") ==  true)
        {
            if (instance.highScore > PlayerPrefs.GetInt("highScore"))
            {
                int newHighScore = instance.highScore;

                Debug.Log("Your about to set " + newHighScore + " as your high score");

                PlayerPrefs.SetInt("highScore", newHighScore);
                PlayerPrefs.Save();
            }
        }
        else
        {
            
                int newHighScore = instance.highScore;

                Debug.Log("Your about to set " + newHighScore + " as your high score");

                PlayerPrefs.SetInt("highScore", newHighScore);
                PlayerPrefs.Save();
            
        }
    }


}


    

