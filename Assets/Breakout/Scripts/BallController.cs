﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class BallController : MonoBehaviour
{
    public ParticleSystem ballParticlePrefab;
    public ParticleSystem paddleParticles;
    List<ParticleSystem> particlePool = new List<ParticleSystem>();
    public float speed = 5f;
    public Rigidbody body;
    

    public AudioSource wubWub;

    // Use this for initialization
    void Start()
    {
        body = GetComponent<Rigidbody>();
        PreLaunch();

        wubWub = GetComponent<AudioSource>();
        wubWub.playOnAwake = false;
        


    }

    void PreLaunch()
    {
        body.velocity = Vector3.zero;
        transform.SetParent(PaddleController.instance.transform);
        transform.localPosition = Vector3.up;
    }

    void Launch()
    {


            transform.SetParent(null);         
            transform.position = PaddleController.instance.transform.position + Vector3.up;

        
            body.velocity = Vector3.up * speed;
        UnityEngine.Debug.Log("launches the ball"); 
        
    }

    void Update()
    {
        
        

        
        if (transform.parent == null)
        {
            Vector3 v = body.velocity;
            if (Mathf.Abs(v.x) > 2 * Mathf.Abs(v.y))
            {
                v.x *= 0.9f;
            }
            v = v.normalized * speed;
            body.velocity = v;

            transform.up = v;
            transform.localScale = new Vector3(.9f, 1.1f, 1);

            DeathCheck();
            WinCheck();
        }else
        {
            transform.localScale = Vector3.one;
            if (Input.GetButton("Jump"))
            {
                Launch();
            }
        }

    }

    void DeathCheck(){
        Vector3 view = Camera.main.WorldToViewportPoint(transform.position);

        if (view.y < 0)
        {
            GameManager.LostBall();
            if(GameManager.instance.lives >= 0)
            {
                PreLaunch();
                
            }else
            {
                gameObject.SetActive(false);
            }
               
        }
    }

    void WinCheck()
    {
        if (GameManager.instance.brickCount == 0)
        {
            gameObject.SetActive(false);
        }
    }

    
    void OnCollisionEnter(Collision c)
    {

        UnityEngine.Debug.Log("Ball Collided with " + c.gameObject.tag);
        ContactPoint cp = c.contacts[0];
        transform.up = cp.normal;
        transform.localScale = new Vector3(1.5f, .5f, 1);

        

        ShakeController shake = Camera.main.gameObject.GetComponent<ShakeController>();
            shake.Shake();
        if (c.gameObject.tag == "Paddle")
        {
            PaddleController.instance.PlayPaddleParticles();
            wubWub.pitch = (Random.Range(0.6f, 0.9f));
            wubWub.Play();
            PaddleController.instance.TextureChanger();
        }
        else
        {

            ParticleSystem hitParticles = null; //= Instantiate(brickParticlePrefab) as ParticleSystem;

            for (int i = 0; i < particlePool.Count; i++)
            {
                ParticleSystem p = particlePool[i];
                if (p.isStopped)
                {
                    hitParticles = p;
                    UnityEngine.Debug.Log("reusing from my pool");
                    break;
                }
            }
            if (hitParticles == null)
            {
                hitParticles = Instantiate(ballParticlePrefab) as ParticleSystem;
                particlePool.Add(hitParticles);
            }
            hitParticles.transform.position = transform.position;

            hitParticles.Play();
        }

       
    }

    //private void OnCollisionExit(Collision collision)
    //{
    //    if (collision.gameObject.tag == "Paddle")
    //    {
    //        Debug.Log("Ball left the paddle");
    //        PaddleController.instance.TextureChanger2();
    //    }
    //}


}
