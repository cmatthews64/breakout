﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleController : MonoBehaviour
{

    public static PaddleController instance;
    public Material material1;
    public Material material2;
    List<ParticleSystem> paddleParticlePool = new List<ParticleSystem>();
    public ParticleSystem waveParticlePrefab;


    Texture startingTexture;
    MaterialPropertyBlock properties;

    public float paddleSpeed = 1f;
    public float tilt = 3;

    Renderer renderer;
    Material materialvar;
    Rigidbody paddleBody;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
         
    }

    void Start()
    {
        renderer = GetComponentInChildren<Renderer>();
        paddleBody = GetComponentInChildren<Rigidbody>();


        renderer.material = material1;
        Debug.Log(renderer.material);
    }

    void Update()
    {

        float x = Input.GetAxis("Horizontal") * paddleSpeed;
        transform.position += Vector3.right * x * Time.deltaTime;
        transform.localEulerAngles = Vector3.back * tilt * x;

        Vector3 v = Camera.main.WorldToViewportPoint(transform.position + Vector3.right * instance.renderer.bounds.extents.x);
        v.x = Mathf.Clamp01(v.x);
        transform.position = Camera.main.ViewportToWorldPoint(v) - Vector3.right * instance.renderer.bounds.extents.x;

        v = Camera.main.WorldToViewportPoint(transform.position + Vector3.left * instance.renderer.bounds.extents.x);
        v.x = Mathf.Clamp01(v.x);
        transform.position = Camera.main.ViewportToWorldPoint(v) - Vector3.left* instance.renderer.bounds.extents.x;
    }

    public void TextureChanger() 
    {
        Debug.Log("TextureChanger() function was called");
        Debug.Log(renderer.material);
        

        Material currentMaterial = renderer.material;
        Debug.Log(currentMaterial.name + "is the currentMAterial.Name");


        if(currentMaterial.name == "PaddleMaterial (Instance)")
        {
            renderer.material = material2;
            currentMaterial = renderer.material;
        }else if(currentMaterial.name == "PaddleMaterial2 (Instance)")
        {
            renderer.material = material1;
            currentMaterial = renderer.material;
        }
        

        
    }

    public void TextureChanger2()
    {
        Debug.Log("TextureChanger() function was called");
        Debug.Log(renderer.material);


        Material currentMaterial = renderer.material;
        Debug.Log(currentMaterial.name + "is the currentMAterial.Name");

        renderer.material = material1;
    }

    public void PlayPaddleParticles()
    {
        ParticleSystem paddleHitParticles = null; //= Instantiate(brickParticlePrefab) as ParticleSystem;

        for (int i = 0; i < paddleParticlePool.Count; i++)
        {
            ParticleSystem p = paddleParticlePool[i];
            if (p.isStopped)
            {
                paddleHitParticles = p;
                UnityEngine.Debug.Log("reusing from my pool");
                break;
            }
        }
        if (paddleHitParticles == null)
        {
            paddleHitParticles = Instantiate(waveParticlePrefab,gameObject.transform) as ParticleSystem;
            paddleParticlePool.Add(paddleHitParticles);
        }
        paddleHitParticles.transform.position = transform.position;

        paddleHitParticles.Play();
        //if (waveParticlePrefab.isEmitting)
        //{
        //    Instantiate(waveParticlePrefab, gameObject.transform);
        //}
        //else
        //{
        //    waveParticlePrefab.Play();
        //}
    }


}


   